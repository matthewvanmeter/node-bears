// server.js

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// =====================================================
// BASIC SETUP
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// =====================================================
// DATABASE
mongoose.connect('mongodb://node:node@ds029638.mongolab.com:29638/node-bears');

// =====================================================
// MODELS
var Bear = require('./app/models/bear.js');

// ======================================================
// ROUTES
var router = express.Router();
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

// ======================================================
// REST ENDPOINTS
router.route('/bears')
    .post(function(req, res) {
        // create a bear
        var bear = new Bear();
        bear.name = req.body.name;

        bear.save(function(err) {
           if (err) {
                req.send(err);
            } 
            res.json(bear);
        })
    })
    .get(function(req, res) {
        Bear.find(function(err, bears) {
            if (err) {
                res.send(err);
            }
            res.json({
                total: bears.length,
                list: bears
            });
        });
    });

router.route('/bears/:id')
    .get(function(req, res) {
        Bear.findById(req.params.id, function(err, bear) {
            if (err) {
                res.send(err);
            }
            res.json(bear);
        });
    })
    .put(function(req, res) {
        Bear.findById(req.params.id, function(err, bear) {
            if (err)
                req.send(err);

            bear.name = req.body.name;

            bear.save(function(err, bear) {
               if (err)
                   res.send(err);

                res.json(bear);
            });
        });
    })
    .delete(function(req, res) {
        Bear.remove({
            _id: req.params.bear_id
        }, function(err, bear) {
            if (err)
                res.send(err);

            res.json({ message: 'Bear removed.' });
        });
    });

// register our routes, all routes will be prefixed with /api
app.use('/api', router);

// =====================================================
// FINISH
var port = process.env.PORT || 3000;
app.listen(port); // run the app
console.log('Magic happens on port ' + port);
